<?php
namespace HIVE\HiveUserfuncs\UserFunc;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class StorageUserFunc
{
    public static function getStoragePidQueryForModelInPlugin($sModel, $sPlugin)
    {
        // get storagePid

        /* @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        /* @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

        $aFullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        /*
         * Integer or comma separated value
         */
        $sStoragePid = $aFullTyposcriptSettings['plugin.'][$sPlugin . '.']['model.'][$sModel . '.']['persistence.']['storagePid'];

        if($sStoragePid != '') {
            return ' AND pid IN (' . $sStoragePid . ')';
        }
        return '';

    }

    public static function getStoragePidQueryForPlugin($sPlugin)
    {
        // get storagePid

        /* @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        /* @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

        $aFullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        /*
         * Integer or comma separated value
         */
        $sStoragePid = $aFullTyposcriptSettings['plugin.'][$sPlugin . '.']['persistence.']['storagePid'];

        if($sStoragePid != '') {
            return ' AND pid IN (' . $sStoragePid . ')';
        }
        return '';

    }

    public static function getStoragePidListForModelInPlugin($sModel, $sPlugin)
    {
        /* @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        /* @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

        $aFullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        /*
         * Integer or comma separated value
         */
        $sStoragePidList = $aFullTyposcriptSettings['plugin.'][$sPlugin . '.']['model.'][$sModel . '.']['persistence.']['storagePid'];

        if($sStoragePidList != '') {
            return (string) $sStoragePidList;
        }
        return '';
    }

    public static function getStoragePidListForPlugin($sPlugin)
    {
        /* @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        /* @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

        $aFullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        /*
         * Integer or comma separated value
         */
        $sStoragePidList = $aFullTyposcriptSettings['plugin.'][$sPlugin . '.']['persistence.']['storagePid'];

        if($sStoragePidList != '') {
            return (string) $sStoragePidList;
        }
        return '';
    }

    public static function getFirstStoragePidForModelInPlugin($sModel, $sPlugin)
    {
        // get storagePid

        /* @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        /* @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

        $aFullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        /*
         * Integer or comma separated value
         */
        $sStoragePidList = $aFullTyposcriptSettings['plugin.'][$sPlugin . '.']['model.'][$sModel . '.']['persistence.']['storagePid'];

        if($sStoragePidList != '') {
            $aPidList = explode(',', $sStoragePidList);
            if (count($aPidList) > 0) {
                $sFirstPid = (string) $aPidList[0];
                return $sFirstPid;
            }
        }
        return '';

    }

    public static function getFirstStoragePidForPlugin($sPlugin)
    {
        // get storagePid

        /* @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        /* @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

        $aFullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        /*
         * Integer or comma separated value
         */
        $sStoragePidList = $aFullTyposcriptSettings['plugin.'][$sPlugin . '.']['persistence.']['storagePid'];

        if($sStoragePidList != '') {
            $aPidList = explode(',', $sStoragePidList);
            if (count($aPidList) > 0) {
                $sFirstPid = (string) $aPidList[0];
                return $sFirstPid;
            }
        }
        return '';

    }
}